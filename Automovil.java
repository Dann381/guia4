import java.util.ArrayList;

public class Automovil {

	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Rueda nuevarueda;
	private Estanque estanque;
	private Boolean encendido;
	String direccion = "Norte";

	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}
	public void getNuevaRueda() {
		this.nuevarueda = nuevarueda;
	}
	public Rueda ObtenerNuevaRueda() {
		return nuevarueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	public void enciende_automovil() {
		
		this.encendido = true;
		Double volumen = this.estanque.getVolumen();
		Double nuevo = volumen - (volumen * 0.01);
		this.estanque.setVolumen(nuevo);
		
	}
	
	public void movimiento() {
	
		System.out.println("El auto se mueve");
	}
	
	public void chequeo_ruedas() {
	
		for (Rueda r : this.rueda) {
			System.out.println(r.getDesgaste());
		}
	}
	
	public void reporte_automovil() {
		System.out.println("El automovil tiene un motor de " + this.motor.getCilindrada());
		System.out.println("El estado actual del estanque es: " + this.estanque.getVolumen());
		System.out.println("La velocidad actual del automovil es " + this.velocimetro.getVelocidad());
		if (this.encendido == false) {
			System.out.println("El automovil se encuentra apagado");
		}
		else {
			System.out.println("El automovil se encuentra encendido");
		}
		System.out.println("Estado de las ruedas: ");
		chequeo_ruedas();

	}
	
}
