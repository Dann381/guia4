
public class Estanque {

	private Double volumen = 32.0;
	private Double consumo = 0.0;

	public Double getVolumen() {
		return volumen;
	}

	public void setVolumen(Double volumen) {
		this.volumen = volumen;
	}
	
	public Double getConsumo() {
		return consumo;
	}
	
	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}
}
