import java.util.Scanner;
import java.util.ArrayList;

public class Controlador {

	
	Controlador(){
		 Scanner input = new Scanner (System.in);
		Automovil a = new Automovil();
		
		/* valor por defecto */
		String cilindrada = "1.2";
		
		/* dos formas distintas de instanciar */
		//Motor m = new Motor(cilindrada);
		Motor m1 = new Motor();
		
		a.setMotor(m1);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		//a.chequeo_ruedas();
		//a.reporte_automovil();
		//a.enciende_automovil();
		cilindrada = a.getMotor().getCilindrada();
		if (cilindrada == "1,2") {
			a.getEstanque().setConsumo(0.05);
		}
		else if (cilindrada == "1,6") {
			a.getEstanque().setConsumo(0.071429);
		}
		
		System.out.println("Cilindrada: " + a.getMotor().getCilindrada());
		System.out.println("Combustible: " + a.getEstanque().getVolumen() + " litros");
		System.out.println("Encendiendo automovil");
		System.out.println("\n");
		a.enciende_automovil();
		int kilometros = 0;
		Boolean viaje = true;
		while (viaje == true) {
			System.out.println("Dirección del movimiento: " + a.getDireccion());
			Boolean respuesta = false;
			while (respuesta == false) {
			respuesta = true;
				System.out.print("Ingrese dirección (1 = norte, 2 = sur, 3 = este, 4 = oeste): ");
				int dir = input.nextInt();
				if (dir == 1) {
					a.setDireccion("Norte");
				}
				else if (dir == 2) {
					a.setDireccion("Sur");
				}
				else if (dir == 3) {
					a.setDireccion("Este");
				}
				else if (dir == 4) {
					a.setDireccion("Oeste");
				}
				else {
					System.out.println("Ingrese un valor válido");
					respuesta = false;
				}
			}
			System.out.println("Velocidad actual: " + a.getVelocimetro().getVelocidad() + " km/h");
			int velocidad = 0;
			Boolean pass = false;
			while (pass == false) {
				System.out.print("Ingrese velocidad del automóvil (km/h): ");
				velocidad = input.nextInt();
				if (velocidad > 120 || velocidad < 0) {
					System.out.println("Ingrese una velocidad válida (entre 0 y 120)");
				}
				else {
					System.out.println("Velocidad correcta");
					pass = true;
				}
			}
			a.getVelocimetro().setVelocidad(velocidad);
			System.out.println("\n> Velocidad actual: " + a.getVelocimetro().getVelocidad() + " km/h");
			System.out.println("> Dirección del movimiento: " + a.getDireccion());
			int tiempo = (int)(Math.random()*11 + 1);
			int distancia = velocidad * tiempo;
			kilometros = kilometros + distancia;
			System.out.println("> Kilometros recorridos: " + kilometros);
			double consumo = a.getEstanque().getConsumo();
			double perdida = consumo * distancia;
			double volumen = a.getEstanque().getVolumen();
			volumen = volumen - perdida;
			a.getEstanque().setVolumen(volumen);
			System.out.println("> Combustible: " + a.getEstanque().getVolumen());
			
			for (Rueda r : a.getRueda()) {
				double porcentaje = (int) (Math.random()*11 + 1);
				porcentaje = porcentaje/100;
				double resta = r.getDesgaste() - (100*porcentaje);
				r.setDesgaste(resta);	
			}
			System.out.println("> Estado ruedas: ");
			a.chequeo_ruedas();
			
			
//			ArrayList<Rueda> rueda;
//			rueda = new ArrayList<Rueda>();
//			rueda = a.getRueda();
//			System.out.println(rueda.get(0));
			
			for (int i = 0; i < a.getRueda().size(); i++) {
				if (a.getRueda().get(i).getDesgaste() <= 10) {
					a.getRueda().remove(i);
					Rueda nuevarueda = a.ObtenerNuevaRueda();
					a.getRueda().add(nuevarueda);
					System.out.println(nuevarueda);
				}
			}
			
			System.out.println("\n");
			if (a.getEstanque().getVolumen() <= 0) {
				System.out.println("Combustible llegó a 0");
				viaje = false;
			}
		}
		System.out.println("\n\nVIAJE TERMINADO");
		
	}
}

